package Windows_Builder;

import java.awt.EventQueue;
import java.awt.event.ContainerListener;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JToolBar;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import java.awt.CardLayout;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;

public class Imitador implements ChangeListener {

	private JFrame frmImitador;
	JRadioButton rdbtnO1,rdbtnO2,rdbtnO3,rdbtnO1C,rdbtnO2C,rdbtnO3C;
	private JTextField textFieldO7,textFieldO7C;
	JCheckBox chckbxO4,chckbxO5,chckbxO6,chckbxO4C,chckbxO5C,chckbxO6C;
	JComboBox comboBox,comboBoxC;
	JSpinner spinner,spinnerC;
	int n ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Imitador window = new Imitador();
					window.frmImitador.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Imitador() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmImitador = new JFrame();
		frmImitador.setTitle("Imitador");
		
		
		
		frmImitador.setBounds(100, 100, 400, 349);
		frmImitador.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frmImitador.getContentPane().setLayout(springLayout);
		
		ButtonGroup grupo = new ButtonGroup();
		ButtonGroup grupoC = new ButtonGroup();
		
		JPanel panelPincipal = new JPanel();
		springLayout.putConstraint(SpringLayout.SOUTH, panelPincipal, 140, SpringLayout.NORTH, frmImitador.getContentPane());
		panelPincipal.setBorder(new TitledBorder(UIManager.getBorder("CheckBoxMenuItem.border"), "Original", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		springLayout.putConstraint(SpringLayout.NORTH, panelPincipal, 10, SpringLayout.NORTH, frmImitador.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panelPincipal, 10, SpringLayout.WEST, frmImitador.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panelPincipal, -10, SpringLayout.EAST, frmImitador.getContentPane());
		frmImitador.getContentPane().add(panelPincipal);
		
		JPanel panelCopia = new JPanel();
		panelCopia.setBorder(new TitledBorder(UIManager.getBorder("CheckBoxMenuItem.border"), "Espejo", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		springLayout.putConstraint(SpringLayout.NORTH, panelCopia, 16, SpringLayout.SOUTH, panelPincipal);
		springLayout.putConstraint(SpringLayout.WEST, panelCopia, 0, SpringLayout.WEST, panelPincipal);
		springLayout.putConstraint(SpringLayout.SOUTH, panelCopia, -10, SpringLayout.SOUTH, frmImitador.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panelCopia, -10, SpringLayout.EAST, frmImitador.getContentPane());
		SpringLayout sl_panelPincipal = new SpringLayout();
		panelPincipal.setLayout(sl_panelPincipal);
		
		rdbtnO1 = new JRadioButton("Opci\u00F3n 1");
		sl_panelPincipal.putConstraint(SpringLayout.NORTH, rdbtnO1, 10, SpringLayout.NORTH, panelPincipal);
		sl_panelPincipal.putConstraint(SpringLayout.WEST, rdbtnO1, 10, SpringLayout.WEST, panelPincipal);
		rdbtnO1.addChangeListener(this);
		panelPincipal.add(rdbtnO1);
		
		rdbtnO2 = new JRadioButton("Opci\u00F3n 2");
		sl_panelPincipal.putConstraint(SpringLayout.NORTH, rdbtnO2, 6, SpringLayout.SOUTH, rdbtnO1);
		sl_panelPincipal.putConstraint(SpringLayout.EAST, rdbtnO2, 0, SpringLayout.EAST, rdbtnO1);
		rdbtnO2.addChangeListener(this);
		panelPincipal.add(rdbtnO2);
		
		rdbtnO3 = new JRadioButton("Opci�n 3");
		sl_panelPincipal.putConstraint(SpringLayout.NORTH, rdbtnO3, 6, SpringLayout.SOUTH, rdbtnO2);
		sl_panelPincipal.putConstraint(SpringLayout.EAST, rdbtnO3, 0, SpringLayout.EAST, rdbtnO1);
		rdbtnO3.addChangeListener(this);
		panelPincipal.add(rdbtnO3);
		frmImitador.getContentPane().add(panelCopia);
		SpringLayout sl_panelCopia = new SpringLayout();
		panelCopia.setLayout(sl_panelCopia);
		
		rdbtnO1C = new JRadioButton("Opci\u00F3n 1");
		rdbtnO1C.setEnabled(false);
		sl_panelCopia.putConstraint(SpringLayout.NORTH, rdbtnO1C, 10, SpringLayout.NORTH, panelCopia);
		sl_panelCopia.putConstraint(SpringLayout.WEST, rdbtnO1C, 10, SpringLayout.WEST, panelCopia);
		panelCopia.add(rdbtnO1C);
		
		rdbtnO2C = new JRadioButton("Opci\u00F3n 2");
		rdbtnO2C.setEnabled(false);
		sl_panelCopia.putConstraint(SpringLayout.NORTH, rdbtnO2C, 6, SpringLayout.SOUTH, rdbtnO1C);
		sl_panelCopia.putConstraint(SpringLayout.EAST, rdbtnO2C, 0, SpringLayout.EAST, rdbtnO1C);
		panelCopia.add(rdbtnO2C);
		
		rdbtnO3C = new JRadioButton("Opci\u00F3n 3");
		rdbtnO3C.setEnabled(false);
		sl_panelCopia.putConstraint(SpringLayout.NORTH, rdbtnO3C, 6, SpringLayout.SOUTH, rdbtnO2C);
		sl_panelCopia.putConstraint(SpringLayout.EAST, rdbtnO3C, 0, SpringLayout.EAST, rdbtnO2C);
		panelCopia.add(rdbtnO3C);
		
		chckbxO4 = new JCheckBox("Opci\u00F3n4");
		sl_panelPincipal.putConstraint(SpringLayout.NORTH, chckbxO4, 0, SpringLayout.NORTH, rdbtnO1);
		sl_panelPincipal.putConstraint(SpringLayout.WEST, chckbxO4, 58, SpringLayout.EAST, rdbtnO1);
		chckbxO4.addChangeListener(this);
		panelPincipal.add(chckbxO4);
		
		chckbxO5 = new JCheckBox("Opci\u00F3n 5");
		sl_panelPincipal.putConstraint(SpringLayout.NORTH, chckbxO5, 0, SpringLayout.NORTH, rdbtnO2);
		sl_panelPincipal.putConstraint(SpringLayout.WEST, chckbxO5, 58, SpringLayout.EAST, rdbtnO2);
		chckbxO5.addChangeListener(this);
		panelPincipal.add(chckbxO5);
		
		chckbxO6 = new JCheckBox("Opci\u00F3n 6");
		sl_panelPincipal.putConstraint(SpringLayout.NORTH, chckbxO6, 0, SpringLayout.NORTH, rdbtnO3);
		sl_panelPincipal.putConstraint(SpringLayout.WEST, chckbxO6, 58, SpringLayout.EAST, rdbtnO3);
		chckbxO6.addChangeListener(this);
		panelPincipal.add(chckbxO6);
		
		chckbxO4C = new JCheckBox("Opci\u00F3n4");
		chckbxO4C.setEnabled(false);
		sl_panelCopia.putConstraint(SpringLayout.NORTH, chckbxO4C, 0, SpringLayout.NORTH, rdbtnO1C);
		sl_panelCopia.putConstraint(SpringLayout.WEST, chckbxO4C, 59, SpringLayout.EAST, rdbtnO1C);
		panelCopia.add(chckbxO4C);
		
		chckbxO5C = new JCheckBox("Opci\u00F3n 5");
		chckbxO5C.setEnabled(false);
		sl_panelCopia.putConstraint(SpringLayout.NORTH, chckbxO5C, 0, SpringLayout.NORTH, rdbtnO2C);
		sl_panelCopia.putConstraint(SpringLayout.WEST, chckbxO5C, 0, SpringLayout.WEST, chckbxO4C);
		panelCopia.add(chckbxO5C);
		
		chckbxO6C = new JCheckBox("Opci\u00F3n 6");
		chckbxO6C.setEnabled(false);
		sl_panelCopia.putConstraint(SpringLayout.NORTH, chckbxO6C, 0, SpringLayout.NORTH, rdbtnO3C);
		sl_panelCopia.putConstraint(SpringLayout.WEST, chckbxO6C, 0, SpringLayout.WEST, chckbxO4C);
		panelCopia.add(chckbxO6C);
		
		grupo.add(rdbtnO1);
		grupo.add(rdbtnO2);
		grupo.add(rdbtnO3);
		grupoC.add(rdbtnO1C);
		grupoC.add(rdbtnO2C);
		grupoC.add(rdbtnO3C);
		
		textFieldO7 = new JTextField();
		textFieldO7.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				textFieldO7C.setText(textFieldO7.getText());
			}
		});
		sl_panelPincipal.putConstraint(SpringLayout.NORTH, textFieldO7, 0, SpringLayout.NORTH, rdbtnO1);
		sl_panelPincipal.putConstraint(SpringLayout.EAST, textFieldO7, -10, SpringLayout.EAST, panelPincipal);
		panelPincipal.add(textFieldO7);
		
		textFieldO7.setColumns(10);
		
		textFieldO7C = new JTextField();
		textFieldO7C.setEnabled(false);
		sl_panelCopia.putConstraint(SpringLayout.NORTH, textFieldO7C, 0, SpringLayout.NORTH, rdbtnO1C);
		sl_panelCopia.putConstraint(SpringLayout.EAST, textFieldO7C, -10, SpringLayout.EAST, panelCopia);
		textFieldO7C.setColumns(10);
		panelCopia.add(textFieldO7C);
		
		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxC.setModel(comboBox.getModel());
			}
		});
		sl_panelPincipal.putConstraint(SpringLayout.NORTH, comboBox, 10, SpringLayout.SOUTH, textFieldO7);
		sl_panelPincipal.putConstraint(SpringLayout.WEST, comboBox, 0, SpringLayout.WEST, textFieldO7);
		sl_panelPincipal.putConstraint(SpringLayout.EAST, comboBox, 0, SpringLayout.EAST, textFieldO7);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"}));
		
		panelPincipal.add(comboBox);
		
		comboBoxC = new JComboBox();
		comboBoxC.setEnabled(false);
		sl_panelCopia.putConstraint(SpringLayout.NORTH, comboBoxC, 0, SpringLayout.NORTH, rdbtnO2C);
		sl_panelCopia.putConstraint(SpringLayout.WEST, comboBoxC, 0, SpringLayout.WEST, textFieldO7C);
		sl_panelCopia.putConstraint(SpringLayout.EAST, comboBoxC, 0, SpringLayout.EAST, textFieldO7C);
		comboBoxC.setModel(new DefaultComboBoxModel(new String[] {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"}));
		comboBoxC.addActionListener(comboBoxC);
		panelCopia.add(comboBoxC);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(n), null, null, new Integer(1)));
		sl_panelPincipal.putConstraint(SpringLayout.WEST, spinner, 0, SpringLayout.WEST, textFieldO7);
		sl_panelPincipal.putConstraint(SpringLayout.SOUTH, spinner, 0, SpringLayout.SOUTH, rdbtnO3);
		sl_panelPincipal.putConstraint(SpringLayout.EAST, spinner, 0, SpringLayout.EAST, textFieldO7);
		spinner.addChangeListener(this);
		panelPincipal.add(spinner);
		
		spinnerC = new JSpinner();
		spinnerC.setEnabled(false);
		//spinnerC.setEnabled(false);
		spinnerC.setModel(new SpinnerNumberModel((n), null, null, new Integer(1)));
		sl_panelCopia.putConstraint(SpringLayout.NORTH, spinnerC, 0, SpringLayout.NORTH, rdbtnO3C);
		sl_panelCopia.putConstraint(SpringLayout.WEST, spinnerC, 0, SpringLayout.WEST, textFieldO7C);
		sl_panelCopia.putConstraint(SpringLayout.EAST, spinnerC, 0, SpringLayout.EAST, textFieldO7C);
		panelCopia.add(spinnerC);
		
		
		
		
		
		
		
		
		
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub
		if (rdbtnO1.isSelected()) {
			rdbtnO1C.setSelected(true);
		}
		if (rdbtnO2.isSelected()) {
			rdbtnO2C.setSelected(true);
		}
		if (rdbtnO3.isSelected()) {
			rdbtnO3C.setSelected(true);
		}
		if(chckbxO4.isSelected()) {
			chckbxO4C.setSelected(true);
		}
		else {
			chckbxO4C.setSelected(false);
		}
		if(chckbxO5.isSelected()) {
			chckbxO5C.setSelected(true);
		}
		else {
			chckbxO5C.setSelected(false);
		}
		if(chckbxO6.isSelected()) {
			chckbxO6C.setSelected(true);
		}
		else {
			chckbxO6C.setSelected(false);
		}
		if(spinner.isShowing()) {
			spinnerC.setValue(spinner.getValue());
		}
		if(comboBox.isShowing()) {
			comboBox.getModel();
			comboBoxC.setModel(comboBox.getModel());
		}
		
		
	}
}












