package Windows_Builder;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.Color;

public class GeneradorDeNumeros implements ChangeListener {

	private JFrame frmGeneradorDeNmeros;
	private JTextField textField;
	JSpinner spinner,spinner_1;
	JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GeneradorDeNumeros window = new GeneradorDeNumeros();
					window.frmGeneradorDeNmeros.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GeneradorDeNumeros() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	public int random(int min, int max) {
		Random rand= new Random();
		int n=0;
		n= rand.nextInt(max-(min-1))+min;
		return n;
		
	}
	
	private void initialize() {
		frmGeneradorDeNmeros = new JFrame();
		frmGeneradorDeNmeros.setTitle("Generador de N\u00FAmeros");
		frmGeneradorDeNmeros.setBounds(100, 100, 350, 350);
		frmGeneradorDeNmeros.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGeneradorDeNmeros.getContentPane().setLayout(new BoxLayout(frmGeneradorDeNmeros.getContentPane(), BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		frmGeneradorDeNmeros.getContentPane().add(panel);
		panel.setLayout(null);
		
		spinner = new JSpinner();
		spinner.setBounds(224, 25, 83, 20);
		spinner.addChangeListener(this);
		panel.add(spinner);
		
		spinner_1 = new JSpinner();
		spinner_1.setBounds(224, 87, 83, 20);
		spinner_1.addChangeListener(this);
		panel.add(spinner_1);
		
		btnNewButton = new JButton("Generar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int spin1=(int) spinner.getValue();
				int spin2=(int) spinner_1.getValue();
				int text = random(spin1,spin2);
				
				textField.setText(String.valueOf(text));
			}
		});
		btnNewButton.setBounds(128, 227, 89, 23);
		btnNewButton.addChangeListener(this);
		panel.add(btnNewButton);
		
		JTextPane txtpnNmero = new JTextPane();
		txtpnNmero.setText("N\u00FAmero 1");
		txtpnNmero.setBackground(SystemColor.menu);
		txtpnNmero.setBounds(34, 25, 89, 20);
		panel.add(txtpnNmero);
		
		JTextPane txtpnNmero_2 = new JTextPane();
		txtpnNmero_2.setText("N\u00FAmero 2");
		txtpnNmero_2.setBackground(SystemColor.menu);
		txtpnNmero_2.setBounds(34, 87, 89, 20);
		panel.add(txtpnNmero_2);
		
		JTextPane txtpnNmeroGenerado = new JTextPane();
		txtpnNmeroGenerado.setText("N\u00FAmero Generado");
		txtpnNmeroGenerado.setBackground(SystemColor.menu);
		txtpnNmeroGenerado.setBounds(34, 151, 109, 20);
		panel.add(txtpnNmeroGenerado);
		
		textField = new JTextField();
		textField.setBackground(Color.WHITE);
		textField.setText("");
		textField.setEditable(false);
		textField.setBounds(221, 151, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub

		
		
	}
}
