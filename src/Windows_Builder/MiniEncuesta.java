package Windows_Builder;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class MiniEncuesta  implements ActionListener{

	private JFrame frame;
	JSlider slider;
	JLabel lblNewLabel;
	JRadioButton rdbtnR1,rdbtnR2,rdbtnR3;
	JButton btnGenerar;
	JCheckBox chckbxProgra,chckbxDiseoGrfico,chckbxAdministracin;
	ButtonGroup grupo;
	String btn1,check1="",check2="",check3="";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MiniEncuesta window = new MiniEncuesta();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MiniEncuesta() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 253, 425);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextPane txtpnEligeUnSistema = new JTextPane();
		txtpnEligeUnSistema.setBackground(SystemColor.menu);
		txtpnEligeUnSistema.setText("Elige un sistema operativo");
		txtpnEligeUnSistema.setBounds(25, 11, 169, 20);
		frame.getContentPane().add(txtpnEligeUnSistema);
		
		rdbtnR1 = new JRadioButton("Windows");
		rdbtnR1.setBounds(35, 38, 109, 23);
		rdbtnR1.addActionListener(this);
		frame.getContentPane().add(rdbtnR1);
		
		rdbtnR2 = new JRadioButton("Linux");
		rdbtnR2.setBounds(35, 64, 109, 23);
		rdbtnR2.addActionListener(this);
		frame.getContentPane().add(rdbtnR2);
		
		rdbtnR3 = new JRadioButton("Mac");
		rdbtnR3.setBounds(35, 90, 109, 23);
		rdbtnR3.addActionListener(this);
		frame.getContentPane().add(rdbtnR3);
		
		grupo = new ButtonGroup();
		grupo.add(rdbtnR1);
		grupo.add(rdbtnR2);
		grupo.add(rdbtnR3);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 127, 217, 2);
		frame.getContentPane().add(separator);
		
		JTextPane txtpnEligeTuEspecialidad = new JTextPane();
		txtpnEligeTuEspecialidad.setText("Elige tu especialidad");
		txtpnEligeTuEspecialidad.setBackground(SystemColor.menu);
		txtpnEligeTuEspecialidad.setBounds(25, 140, 169, 20);
		frame.getContentPane().add(txtpnEligeTuEspecialidad);
		
		chckbxProgra = new JCheckBox(" Programaci\u00F3n");
		chckbxProgra.setBounds(35, 167, 97, 23);
		chckbxProgra.addActionListener(this);
		frame.getContentPane().add(chckbxProgra);
		
		chckbxDiseoGrfico = new JCheckBox(" Dise\u00F1o gr\u00E1fico");
		chckbxDiseoGrfico.setBounds(35, 193, 97, 23);
		chckbxDiseoGrfico.addActionListener(this);
		frame.getContentPane().add(chckbxDiseoGrfico);
		
		chckbxAdministracin = new JCheckBox(" Administraci\u00F3n");
		chckbxAdministracin.setBounds(35, 219, 97, 23);
		chckbxAdministracin.addActionListener(this);
		frame.getContentPane().add(chckbxAdministracin);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 251, 217, 2);
		frame.getContentPane().add(separator_1);
		
		JTextPane txtpnHorasQueDedicas = new JTextPane();
		txtpnHorasQueDedicas.setText("Horas que dedicas en el ordenador");
		txtpnHorasQueDedicas.setBackground(SystemColor.menu);
		txtpnHorasQueDedicas.setBounds(25, 262, 189, 20);
		frame.getContentPane().add(txtpnHorasQueDedicas);
		

		
		
		slider = new JSlider(0,12,0);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				lblNewLabel.setText(String.valueOf(slider.getValue()));
			}
		});
		slider.setBounds(58, 293, 169, 20);
		//slider.add
		frame.getContentPane().add(slider);
		
		lblNewLabel = new JLabel(String.valueOf(slider.getValue()));
		//lblNewLabel.setText(String.valueOf(slider.getValue()));
		lblNewLabel.setBounds(10, 299, 46, 14);
		//lblNewLabel.add
		frame.getContentPane().add(lblNewLabel);
	
		btnGenerar = new JButton("Generar");
		btnGenerar.setBounds(73, 339, 89, 23);
		btnGenerar.addActionListener(this);
		frame.getContentPane().add(btnGenerar);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
		if (arg0.getSource()==btnGenerar) {
			//JOptionPane.showMessageDialog(null, "alert", "alert", JOptionPane.ERROR_MESSAGE);
			JOptionPane.showMessageDialog(null,"Tu sistema operativo preferido es "+ btn1+
					", tus especialidades son "+check1+check2+check3+" y el n�mero de horas dedicadas al ordenador son "+lblNewLabel.getText() );
		}
		if (rdbtnR1.isSelected()) {
			btn1=rdbtnR1.getText();
		}
		if (rdbtnR2.isSelected()) {
			btn1=rdbtnR2.getText();
		}
		if (rdbtnR3.isSelected()) {
			btn1=rdbtnR3.getText();
		}
		if (chckbxProgra.isSelected()) {
			check1=chckbxProgra.getText();
		}
		else {
			check1="";
		}
		if (chckbxDiseoGrfico.isSelected()) {
			check2=chckbxDiseoGrfico.getText();
		}
		else {
			check2="";
		}
		if (chckbxAdministracin.isSelected()) {
			check3=chckbxAdministracin.getText();
		}
		else {
			check3="";
		}
		
	}
}
