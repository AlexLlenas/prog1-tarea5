package Windows_Builder;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.SystemColor;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionEvent;

public class Peliculas implements ActionListener {

	private JFrame frame;
	private JTextField textField;
	JButton btnNewButton;
	JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Peliculas window = new Peliculas();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Peliculas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 160);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextPane txtpnEscribeElTtulo = new JTextPane();
		txtpnEscribeElTtulo.setBackground(SystemColor.menu);
		txtpnEscribeElTtulo.setText("Escribe el t\u00EDtulo de una pel\u00EDcula");
		txtpnEscribeElTtulo.setBounds(40, 11, 168, 20);
		frame.getContentPane().add(txtpnEscribeElTtulo);
		
		textField = new JTextField();
		textField.setBounds(40, 42, 153, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		 btnNewButton = new JButton("A\u00F1adir");
		btnNewButton.setBounds(74, 73, 89, 23);
		btnNewButton.addActionListener(this);
		frame.getContentPane().add(btnNewButton);
		
		JTextPane txtpnPelculas = new JTextPane();
		txtpnPelculas.setText("Pel\u00EDculas");
		txtpnPelculas.setBackground(SystemColor.menu);
		txtpnPelculas.setBounds(284, 11, 65, 20);
		frame.getContentPane().add(txtpnPelculas);
		
		 comboBox = new JComboBox();
		//comboBox.addItem("hola");
		comboBox.setBounds(243, 42, 153, 20);
		frame.getContentPane().add(comboBox);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if (e.getSource()==btnNewButton) {
			System.out.println("hi");
			comboBox.addItem(textField.getText());
		}
		
	}
}
